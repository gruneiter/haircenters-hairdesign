import $ from 'jquery';
import fancybox from '@fancyapps/fancybox';
import fotorama from '../../node_modules/fotorama/fotorama';
import tabs from '../blocks/tabs/tabs.js';
import headerMobileSwitcher from '../blocks/header/header-mobile-switcher/header-mobile-switcher.js';
import headerNavigation from '../blocks/header/header-navigation/header-navigation.js';

tabs();
headerMobileSwitcher();
headerNavigation();
