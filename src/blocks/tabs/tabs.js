export default function tabs () {
  const tabs = document.querySelectorAll( '.tabs' );
  for ( let block of tabs ) {
      const tl = Array.from( block.querySelectorAll( '.tabs__label' ) );
      const tc = Array.from( block.querySelectorAll( '.tab' ) );
      for ( let i in tl ) {
        if ( i == 0 ) {
          tl[i].classList.add( 'tabs__label--active' );
          tc[i].classList.add( 'tab--active' );
        }
        tl[i].addEventListener( 'click', (e) => {
          if ( !tc[i].classList.contains('tab--active')) {
            for ( let j in tl ) {
              tc[j].classList.remove( 'tab--active' );
              tl[j].classList.remove( 'tabs__label--active' );
            }
            tc[i].classList.add( 'tab--active' );
            tl[i].classList.add( 'tabs__label--active' );
          }
        } )
      }
  }

}
