export default function headerMobileSwitcher() {
  const  hms = document.querySelector('.header-mobile-switcher');
  hms.addEventListener('click', (e) => {
    e.target.nextElementSibling.classList.toggle('header-navigation--active');
  } );
}
