export default function headerNavigation() {
  headerNavigationSublistSwitcher();
}

function headerNavigationSublistSwitcher() {
  const hnss = document.querySelectorAll( '.header-navigation__sublist-switch' );
  for ( let item of hnss ) {
    item.addEventListener( 'click', (e) => {
        e.target.classList.toggle('header-navigation__sublist-switch--active');
        e.target.nextElementSibling.classList.toggle('header-navigation__sublist--active');
    } );
  }
}
