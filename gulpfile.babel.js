import gulp from 'gulp';
import gulpIf from 'gulp-if';
import stylus from 'gulp-stylus';
import pug from 'gulp-pug';
import stylint from 'gulp-stylint';
import cssNano from 'gulp-cssnano';
import sourcemaps from 'gulp-sourcemaps';
import browserSync from 'browser-sync';
import watch from 'gulp-watch';
import gcmq from 'gulp-group-css-media-queries';
import changed from 'gulp-changed';
import autoprefixer from 'autoprefixer-stylus';
import importIfExist from 'stylus-import-if-exist';
import plumber from 'gulp-plumber';
import webpack from 'webpack';
import webpackStream from 'webpack-stream';
import webpackConf from './webpack.config.js';
import spritesmith from 'gulp.spritesmith-multi';
import merge from 'merge-stream';
import stylusSvgImport from 'stylus-svg';

var reload = browserSync.reload;

const isDebug = process.env.NODE_ENV !== 'production';

const distDir = './dist';
const templateDir = `${distDir}/template/`;

gulp.task('stylus', () => {
  gulp.src('./src/styles/*.styl')
    .pipe(plumber())
    .pipe(gulpIf(isDebug, sourcemaps.init()))
    .pipe(stylus({
      use: [
        stylusSvgImport(),
        importIfExist(),
        autoprefixer()
      ],
      'include css': true
    }))
    .pipe(gulpIf(!isDebug, gcmq()))
    .pipe(gulpIf(!isDebug, cssNano()))
    .pipe(gulpIf(isDebug, sourcemaps.write()))
    .pipe(gulp.dest(`${templateDir}styles`))
    .pipe(reload({stream:true}));
});


gulp.task('pug', () => {
  gulp.src('./src/pages/*.pug')
    .pipe(plumber())
    .pipe(pug( {
      basedir: 'src',
      pretty: true
    } ))
    .pipe(gulp.dest(distDir))
    .pipe(reload({stream:true}));
});

gulp.task('copy', () => {
  gulp.src(['src/resources/**'])
    .pipe(changed(distDir))
    .pipe(gulp.dest(distDir))
    .pipe(reload({stream:true}));
  gulp.src('src/template/**')
    .pipe(changed(`${templateDir}`))
    .pipe(gulp.dest(`${templateDir}`))
    .pipe(reload({stream:true}));
});

gulp.task('spritesmith', () => {
  let spriteData = gulp.src('./src/sprites/**/*.png')
    .pipe(plumber())
    .pipe(spritesmith({
      spritesmith(options, sprite) {
        options.cssName = sprite + '.styl';
        options.cssFormat = 'stylus';
        options.cssTemplate = './node_modules/spritesheet-templates/lib/templates/stylus.template.handlebars';
      }
    }))
  let imgStream = spriteData.img
    .pipe(gulp.dest(`${templateDir}images/sprites/`))
  let cssStream = spriteData.css
    .pipe(gulp.dest('./src/styles/sprites/'));
  return merge(imgStream, cssStream).pipe(reload({stream:true}));
})

gulp.task('script', () => {
  gulp.src('./src/scripts/script.js')
    .pipe(plumber())
    .pipe(webpackStream(webpackConf, webpack))
    .pipe(gulp.dest(`${templateDir}scripts/`))
    .pipe(reload({stream:true}));
})

gulp.task('watcher', () => {
  watch(['./src/styles/*.styl','./src/styles/includes/*', './src/styles/svg/*.svg', './src/blocks/**/*.styl', './src/blocks/**/**/*.styl'], () => gulp.start('stylus'));
  watch(['./src/blocks/**/*.pug', './src/blocks/**/**/*.pug', './src/pages/*.pug', './src/data/*'], () => gulp.start('pug'));
  watch(['./src/blocks/**/*.js', './src/scripts/script.js'], () => gulp.start('script'));
  watch(['./src/resources/**','src/template/**'], () => gulp.start('copy'));
  watch('./src/sprites/**/*.png', () => gulp.start('spritesmith'));
});

gulp.task('build', ['spritesmith', 'stylus', 'pug', 'script', 'copy'])

gulp.task('browserSync', ()  => {
  browserSync({
    server: {
      baseDir: [
        "dist"
      ]
    },
    open: false,
    notify: false
  });
});

gulp.task('default', ['spritesmith', 'stylus', 'pug', 'script',  'copy', 'watcher', 'browserSync']);
